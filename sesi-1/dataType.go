package main

import "fmt"

func main() {
	var number1 = 89
	var number2 = -12
	var floatNumber float32 = 1.111111
	var floatNumber1 float32 = 1.11111111
	var boolean bool = true

	var default1 int
	var default2 string
	var default3 float32
	var message = `Selamat datang "semua".Salam kenal`

	fmt.Printf("satu %T\n. dua %T\n", number1, number2)
	fmt.Print(boolean)
	fmt.Println(floatNumber)
	fmt.Println(floatNumber1)
	fmt.Println(message)
	fmt.Printf("Default int %d, Default string %s, Default float %g", default1, default2, default3)
}
